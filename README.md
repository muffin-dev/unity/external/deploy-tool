# Muffin Dev for Unity - Unity Deploy Tool

This Node JS package provide automations for creating, updating and deploying Unity packages.

The package includes both an API and a CLI, so you can use it without installation using the `npx` command or by installing it globally or locally with `npm`:

```bash
npm i -g @muffin-dev/unity-deploy-tool
```

## CLI Usage

Every commands can trigger a wizard in the terminal (using [`inquirer`](https://www.npmjs.com/package/inquirer)) if some informations are missing. In automated process, this wizard can be skipped by using the `-s` (for *silent mode*) option.

### `create`

This command generates the minimum layout of a Unity package, following the Unity manual's guidelines: https://docs.unity3d.com/Manual/cus-layout.html

```bash
unity-deploy-tool create [directory] [...options]
```

#### Arguments

- `[directory]`: The directory of the package you want to create. If the given path is relative, it's resolved from the shell's working directory. If not given, the wizard will ask you this information.

#### Options

- `--authorEmail`: The email address of the package's author. If defined, overrides the `author.email` property from the deployment config file (see *Deployment Config File* section below).
- `--authorName`: The full name of the package's author. If defined, overrides the `author.name` property from the deployment config file (see *Deployment Config File* section below).
- `--authorUrl`: The website URL of the package's author. If defined, overrides the `author.url` property from the deployment config file (see *Deployment Config File* section below).
- `--company`: The name of the company that provides the package. If defined, overrides the `company` property from the deployment config file (see *Deployment Config File* section below).
- `--config`: The path to the deployment config file to use (see *Deployment Config File* section below). If relative path given, it's resolved from the shell's working directory. If not defined, the process will try to find that file in the shell's working directory.
- `--description`: The description of the package.
- `--displayName`: The display name of the package, as displayed in the *Packages Manager*.
- `--name`: The unique name of the package, using the *com* notation: `com.company.product`.
- `-s, --silent`: Enables silent mode. The process will assume that all arguments are defined correctly, so the wizard won't spawn.
- `--unity`: The minimum required Unity version to use the package.
- `--version`: The version of the package.

### `update`

This command updates an existinng Unity package, and also automatize the package's Git repository operations.

```bash
unity-deploy-tool update [directory] [...options]
```

With all arguments setup, this command will do the following operations:

- Update the `package.json` file of the package to increment the `version` property.
- Commit the changes
- Add a tag to that commit
- Push the changes to the server

***IMPORTANT**: This command uses `git` features, so you must have Git SCM installed: [https://git-scm.com](https://git-scm.com/)*

#### Arguments

- `[directory]`: The directory of the package you want to update. If the given path is relative, it's resolved from the shell's working directory. If not given, the wizard will ask you this information.

#### Options

- `--commit`: The message of the update changes commit. If not defined, the changes won't be commited on the repository.
- `-p, --push`: If enabled, the update changes are pushed. Only used if a commit message is defined.
- `-s, --silent`: Enables silent mode. The process will assume that all arguments are defined correctly, so the wizard won't spawn.
- `--severity`: Possible values are `major`, `minor` and `patch`. It defines the *severity* of the update, matching the [*SemVer*](https://semver.org) specifications: `major` means breaking changes, `minor` means new features or non-breaking changes, and `patch` means improvements and bug fixes.
- `--tag`: The message of the update changes commit's tag. If not defined, no tag will be added to that commit. Only used if a commit message is defined.
- `--tagName`: The custom name of the update changes commit's tag. If not defined, use `package-name/MAJOR.x/MAJOR.MINOR.PATCH` as tag name by default. Only used if a tag message is defined.

### `deploy`

This command deploys existing Unity packages. "Deploying" a Unity package only means copying packages files into defined targets (basically, the projects you're working on locally).

```bash
unity-deploy-tool deploy [directories] [...options]
```

This command mostly uses the the deployment config file (see *Deployment Config File* section below) to get the informations about the packages you want to deploy. Since this setup would be insane through command options, you can only use that config file for automations, or interact with the wizard.

***IMPORTANT**: This command uses `dotnet` features, so you must have the .NET framework installed: [https://dotnet.microsoft.com/en-us/download/dotnet-framework](https://dotnet.microsoft.com/en-us/download/dotnet-framework)*

@todo The wizard is currently WIP for this command, sorry!

#### Arguments

- `[directories]`: The directory (or directories, separated by commas or semicolons) of the package(s) you want to deploy. If a given path is relative, it's resolved from the shell's working directory.

#### Options

- `--config`: The path to the deployment config file to use (see *Deployment Config File* section below). If relative path given, it's resolved from the shell's working directory. If not defined, the process will try to find that file in the shell's working directory.
- `-s, --silent`: Enables silent mode. The process will assume that all arguments are defined correctly, so the wizard won't spawn.

## Deployment Config File

You can create a `.deployrc` file in the directory that contain your packages, or use the `--config` option of the CLI commands to set the path to that file. This file prevent you to rewrite options that are commonly used (such as the `company` and `author` infos), but may also be required for some commands, as it's the case for `deploy`.

Here is the anexample of the full content of a `.deployrc` file:

```json
{
  "author": {
    "name": "Firstname Lastname",
    "email": "your@email.com",
    "url": "https://your-website.com"
  },
  "company": "YourCompany",
  "vsSolution": "../PackagesVSSolution",
  "managedDlls": [
    "path/to/Unity/installs/2020.3.30f1/Editor/Data/Managed"
  ],
  "deploy": [
    {
      "path": "./MyPackage",
      "dllTargetFramework": "netstandard2.0",
      "excludeFiles": [],
      "targets": [
        {
          "dll": true,
          "clear": true,
          "paths": [
            "D:/MyProjects/Project_01/Packages/MyPackage",
            "D:/MyProjects/Project_02/Packages/MyPackage"
          ]
        }
      ]
    }
  ]
}
```

- `author`: Informations about the author of the packages.
  - `name`: The full name of the author.
  - `email`: The email address of the author.
  - `url`: The website URL of the author.
- `company`: The name of the company that provides the packages.
- `vsSolution`: The path to the Visual Studio solution generated to build packages as DLLs when required.
- `managedDlls`: The paths to DLLs or directories that contain DLLs that may be used as references by the generated package DLLs.
- `deploy`: Instructions about how and where the packages are deployed when using the `deploy` command.
  - `path`: The path to the package for which you're defining the deployment instructions. If the path is relative, it's resolved from the `.deployrc` file's directory.
  - `dllTargetFramework`: The target .NET version to use for the package's DLL. Recommended values are `netstandard2.0` for maximum compatibility, `netstandard 2.1` for Unity 2021+, or `net40` if you need more features than the standard framework.
  - `excludeFiles`: The list of all the files that should not be copied to the output targets. If a path is relative, it's resolved from the package's directory.
  - `targets`: Dpeloy instructions for all the directories where the package will be deployed.
    - `dll`: If enabled, a DLL will be generated instead of just copying scripts in the output targets.
    - `clear`: If enabled, the target directories will be cleared before any file is generated in it.
    - `paths`: The path (or array of paths) to the directories where the package is deployed.

## Future Improvements

- A `next` command to compute the next version of a package or the update changes tag, based on the severity of an update

## Limitations

- Again, this package is meant to work with Unity packages only. If you need a tool like this for other kind of framework packages, please contact us at contact@muffindev.com.
- The `deploy` command only acts locally for now, but we plan to provide configuration to upload packages online as tarballs (`*.tgz` archives).

---

Copyright Muffin Dev © 2022