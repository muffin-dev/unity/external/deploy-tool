export const SEMVER_REGEX = /(?<major>\d+)(.(?<minor>\d+)(.(?<patch>\d+))?)?/;

/**
 * @class Represets the informations about a version with SemVer notation (major.minor.patch).
 */
export class SemVerInfo {

  public major = 0;
  public minor = 0;
  public patch = 0;

  /**
   * Parses a version string using SemVer notation (major.minor.patch) into distinct numbers.
   * @param version The version string you want to parse.
   */
  public static parse(version: string): ISemVerInfo {
    const semver: ISemVerInfo = {
      major: 0,
      minor: 0,
      patch: 0
    };
  
    const match = version.match(SEMVER_REGEX);
    if (match && match.groups) {
      semver.major = parseInt(match.groups.major) || 0;
      semver.minor = parseInt(match.groups.minor) || 0;
      semver.patch = parseInt(match.groups.patch) || 0;
    }
    return semver;
  }

}

/**
 * @interface ISemVerInfo Represets the informations about a version with SemVer notation (major.minor.patch).
 */
export interface ISemVerInfo extends Partial<SemVerInfo> { }