import { exec } from 'child_process';
import { readdir, rm, existsSync, promises } from 'fs';
import { join } from 'path';
import { asAbsolute } from '@muffin-dev/node-helpers';

export * from './semver';

export const DEFAULT_COMPANY_NAME = 'Company';
export const SPLIT_SEMICOLON_REGEX = /[\s\r\n]*[,;][\s\r\n]*/;

/**
 * Normalizes the given text into lowercase, and replace spaces by dashes.
 * @param text The text you want to normalize.
 * @returns Returns the normalized text.
 */
export function normalize(text: string) {
  return text.trim().toLowerCase().replace(' ', '-');
}

/**
 * Executes a command in child process.
 * @param command The command you want to execute.
 * @param ignoreWarnings If enabled, all error messages that start with "warning" is ignored.
 * @returns Returns the command output.
 */
export function execAsync(command: string, cwd: string = null, ignoreWarnings = true) {
  return new Promise<string>((resolve, reject) => {
    exec(command, {
      cwd: cwd ? cwd.trim() : process.cwd()
    }, (error, stdout, stderr) => {
      if (error) {
        console.error('Exec async error:', error);
        reject(error.message);
        return;
      }

      if (stderr) {
        if (!ignoreWarnings || !stderr.toLowerCase().startsWith('warning')) {
          console.error('Exec async outputs error:', stderr);
          reject(stderr);
          return;
        }
      }

      resolve(stdout);
    });
  });
}

/**
 * Splits the given text by semicolons, and get rid of spaces or carriage return before and after them.
 * @param text The text you want to split.
 * @returns Returns the splitted text.
 */
export function splitSemicolon(text: string): string[] {
  return text ? text.split(SPLIT_SEMICOLON_REGEX) : new Array<string>();
}

/**
 * Clears the content of a directory.
 * @param path The path to the directory you want to clear.
 * @param removeRoot If enabled the container is also removed.
 */
export async function clearDirectoryAsync(path: string, removeRoot = false) {
  path = asAbsolute(path);
  if (!existsSync(path)) {
    return;
  }

  const files = await promises.readdir(path);
  for (const f of files) {
    await promises.rm(join(path, f), { recursive: true });
  }
  
  if (removeRoot) {
    await promises.rm(path)
  }
}