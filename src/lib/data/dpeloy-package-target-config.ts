import { asAbsolute } from '@muffin-dev/node-helpers';
import { splitSemicolon } from '..';

/**
 * @class Represents a local target for a package to deploy.
 */
 export class DeployPackageTargetConfig {

  /**
   * @prop If enabled, the package will be deployed as DLL.
   */
  public dll = false;

  /**
   * @prop If enabled, the target directory (or directories) will be cleared before copying the package files.
   */
  public clear = false;

  /**
   * @prop The path (or paths) to the target directory (or directories) where the package will be deployed.
   */
  private _paths = new Array<string>();

  /**
   * Gets the path(s) to the target directory (or directories) where the package will be deployed.
   */
  public get paths() {
    if (this._paths && !Array.isArray(this._paths)) {
      this._paths = splitSemicolon(this._paths);
    }
    return this._paths;
  }

  /**
   * Sets the path (or paths) to the target directory (or directories) where the package will be deployed.
   * @param value The target path(s). If string given, you can pass several directtories by separating them with a semicolon (;).
   */
  public set paths(value: string | string[]) {
    this._paths = Array.isArray(value) ? value : splitSemicolon(value);
  }

}

/**
 * @interface IDeployPackageTargetConfig Represents a local target for a package to deploy.
 */
export interface IDeployPackageTargetConfig extends Partial<DeployPackageTargetConfig> { }