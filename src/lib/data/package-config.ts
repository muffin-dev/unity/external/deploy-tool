import { asAbsolute, readJSONFileAsync } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { AuthorInfo } from '.';
import { PACKAGE_JSON } from '..';

/**
 * @class Informations about a Unity package. Follows the layout rules from Unity manual:
 * https://docs.unity3d.com/Manual/cus-layout.html
 */
export class PackageConfig {

  //#region Properties

  /**
   * @prop The unique name of the package.
   */
  public name: string = null;

  /**
   * @prop The version of the package.
   */
  public version: string = null;

  /**
   * @prop The description of the package.
   */
  public description: string = null;

  /**
   * @prop The name of the package, as displayed in the Package Manager.
   */
  public displayName: string = null;

  /**
   * @prop The minimum required Unity version to use this package.
   */
  public unity: string = null;

  /**
   * @prop Informations about the author of the package.
   */
  public author = new AuthorInfo();

  /**
   * @prop if enabled, the package can be hidden to the user in the Project view.
   */
  public hideInEditor = false;

  //#endregion

  //#region Public API

  /**
   * Reads a package.json file, and outputs a PackageConfig instance.
   * @param path The path to the package.json file.
   */
  public static async read(path: string): Promise<PackageConfig> {
    if (!path.endsWith(PACKAGE_JSON)) {
      path = join(path, PACKAGE_JSON);
    }
    path = asAbsolute(path);
    
    const packageConfig = new PackageConfig();
    const packageConfigFileContent = await readJSONFileAsync(path, null, false);
    Object.assign(packageConfig, packageConfigFileContent);
    return packageConfig;
  }

  //#endregion

}

/**
 * @interface IPackageConfig Informations about a Unity package. Follows the layout rules from Unity manual:
 * https://docs.unity3d.com/Manual/cus-layout.html
 */
export interface IPackageConfig extends Partial<PackageConfig> { }