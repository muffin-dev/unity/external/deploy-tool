/**
 * @class Informations about the author of a package.
 */
export class AuthorInfo {
  /**
   * @prop The name of the author.
   */
  public name: string = null;

  /**
   * @prop The email address of the author.
   */
  public email: string = null;

  /**
   * @prop The website URL of the author.
   */
   public url: string = null;
}