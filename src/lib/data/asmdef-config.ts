const EDITOR_PLATFORM = 'Editor'

/**
 * @class The configuration of an *.asmdef (Assembly Definition) file.
 */
export class AsmdefConfig {
  /**
   * @prop The name of the assembly.
   */
  public name: string = null;

  /**
   * @prop The namespace of the elements created in this assembly.
   */
  public rootNamespace: string = null;

  /**
   * @prop The list of other *.asmdef references in the project.
   */
  public references = new Array<string>();

  /**
   * @prop The list of platforms for which this assmebly can be used.
   */
  public includePlatforms = new Array<string>();
  
  /**
   * @prop The list of platforms for which this assmebly can't be used.
   */
  public excludePlatforms = new Array<string>();

  /**
   * @prop If enabled, unsafe C# blocks are allowed.
   */
  public allowUnsafeCode = false;

  /**
   * @prop
   */
  public overrideReferences = false;

  /**
   * @prop
   */
  public autoReferenced = true;

  /**
   * @prop
   */
  public noEngineReferences = false;

  /**
   * @prop The list of DLLs references in the project.
   */
  public precompiledReferences = new Array<string>();

  /**
   * @prop
   */
  public defineConstraints = new Array<string>();

  /**
   * @prop
   */
  public versionDefines = new Array<string>();

  /**
   * Class constructor.
   * @param name The name of the Assembly Definition.
   * @param editorOnly If enabled, initializes the Assembly Definition with the Editor for only included platform.
   */
  constructor(name: string = null, editorOnly = false) {
    this.name = name;
    if (editorOnly) {
      this.includePlatforms.push(EDITOR_PLATFORM);
    }
  }
}

/**
 * @interface IAsmdefConfig The configuration of an *.asmdef (Assembly Definition) file.
 */
export interface IAsmdefConfig extends Partial<AsmdefConfig> {}
