import { existsSync, lstatSync } from 'fs';
import { basename } from 'path';
import { asAbsolute, readdirAsync } from '@muffin-dev/node-helpers';
import { TTargetDotNetFramework } from '..';
import { DeployPackageTargetConfig } from '.';

/**
 * @class Settings for deploying a package, as written in the "deploy" property of a deployment config file.
 * This is meant to be "local" settings, in the deployment config file of a single package.
 */
export class DeployPackageConfig {

  /**
   * @prop The path to the package you're confiiguring the deployment instructions.
   */
  public path: string = null;

  /**
   * @prop Defines the target framework of the DLL to generate.
   */
  public dllTargetFramework: TTargetDotNetFramework = 'netstandard2.0';

  /**
   * @prop Paths to all the files that won't be copied to the targets when the package is deployed.
   */
  public excludeFiles = new Array<string>();

  /**
   * @prop Informations about how and where packages are exported.
   */
  public targets = new Array<DeployPackageTargetConfig>();

}

/**
 * @interface IDeployPackageConfig Settings for deploying a package, as written in the "deploy" property of a deployment config file.
 * This is meant to be "local" settings, in the deployment config file of a single package.
 */
export interface IDeployPackageConfig extends Partial<DeployPackageConfig> { }