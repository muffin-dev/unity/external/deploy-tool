import { existsSync, lstatSync } from 'fs';
import { join, basename } from 'path';
import { asAbsolute, readdirAsync, readJSONFileAsync } from '@muffin-dev/node-helpers';
import { overrideObject } from '@muffin-dev/js-helpers';
import { AuthorInfo, DeployPackageConfig } from '.';

/**
 * @constant DEPLOY_CONFIG The name of deployment config file.
 */
export const DEPLOY_CONFIG = '.deployrc';

/**
 * @class Reprsents the content of a deployment config file.
 */
export class DeployConfig {

  //#region Properties

  /**
   * @prop The name of the company that provide the packages.
   */
  public company: string = null;

  /**
   * @prop The informations about the author that provide the packages.
   */
  public author: AuthorInfo = new AuthorInfo();

  /**
   * @prop The path to the Visual Solution that will be generated to build packages' DLLs.
   */
  public vsSolution: string = null;

  /**
   * @prop Paths to the managed DLLs (such as UnityEngine.dll) that may be used as references for package DLLs. Those paths can be both
   * path to directories containing *.dll files, or direct *.dll file path.
   */
  public managedDlls = new Array<string>();

  /**
   * @prop Informations about the deployment of a package. This is meant to be a "local" config, for a single package.
   */
  public deploy = new Array<DeployPackageConfig>();

  //#endregion

  //#region Public API

  /**
   * Reads a deployment config file, and outputs a DeployConfig instance.
   * @param path The path to the deployment config file.
   */
  public static async read(path: string): Promise<DeployConfig> {
    if (!path.endsWith(DEPLOY_CONFIG)) {
      path = join(path, DEPLOY_CONFIG);
    }
    path = asAbsolute(path);
    
    const deployConfig = new DeployConfig();
    const deployConfigFileContent = await readJSONFileAsync(path, null, false);
    overrideObject(deployConfig, deployConfigFileContent);
    return deployConfig;
  }

  //#endregion

}
/**
 * @interface IDeployConfig Reprsents the content of a deployment config file.
 */
export interface IDeployConfig extends Partial<DeployConfig> { }