import { relative, join, resolve } from 'path';
import { lstatSync, readdirSync, rmdirSync, rmSync } from 'fs';
import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute, copyFileAsync, readdirAsync } from '@muffin-dev/node-helpers';
import { DeployConfig, DeployPackageConfig, IDeployConfig, VisualStudioProject, VisualStudioSolutionBuilder, splitSemicolon } from '.';
import { clearDirectoryAsync } from './utilities';

const EXCLUDED_SUFFIXES_IF_DLL_TARGET = [
  '.cs',
  '.cs.meta',
  '.asmdef',
  '.asmdef.meta'
]

/**
 * Deploys the packages from the given directory.
 * @param path The path(s) to the directory (or directories) of the package(s) you want to deploy.
 * @param config The path to a deployment config file or the direct configuration to use to deploy packages.
 */
export async function deploy(paths: string | string[], config: string | IDeployConfig = null) {
  // Fix config
  if (!config) {
    config = new DeployConfig();
  }
  if (typeof config === 'string') {
    config = await DeployConfig.read(asAbsolute(config));
  }

  // Fix paths
  if (!Array.isArray(paths)) {
    paths = splitSemicolon(paths);
  }
  for (let i = 0; i < paths.length; i++) {
    paths[i] = asAbsolute(paths[i]);
  }

  let requireDll = false;
  // For each package the user wants to deploy (from the end of the list)
  for (let i = paths.length - 1; i >= 0; i--) {
    let hasDeployConfig = false;
    // Check if that package is referenced in the deployment instructions
    for (const deployPackageConfig of config.deploy) {
      if (asAbsolute(deployPackageConfig.path) === paths[i]) {
        // If at least one package requires to generate DLLs, enable the flag for generating the VS solution
        if (!requireDll && deployPackageConfig.targets.find(t => t.dll)) {
          requireDll = true;
        }

        hasDeployConfig = true;
        break;
      }
    }

    // Skip if the current package doesn't have a deployment config
    if (!hasDeployConfig) {
      console.warn(`No deployment instructions can be found for the package at ${paths[i]}, so it will be ignored.`);
      paths.splice(i, 1);
      continue;
    }
  }

  if (paths.length <= 0) {
    console.warn('No packages to deploy.');
    return;
  }

  // At this step, we are sure that packages all have an appropriate deployment configuration.

  const vsSolution = requireDll ? new VisualStudioSolutionBuilder() : null;
  // For each package to deploy
  for (const p of paths) {
    // Get the package's deployment configuration
    const deployPackageConfig = new DeployPackageConfig();
    overrideObject(deployPackageConfig, config.deploy.find(i => asAbsolute(i.path) === p));

    // Fix target paths and clear target directories if required
    for (const targetConfig of deployPackageConfig.targets) {
      // Fix target paths
      if (!Array.isArray(targetConfig.paths)) {
        targetConfig.paths = splitSemicolon(targetConfig.paths);
      }

      // Clears the target directory if required
      if (targetConfig.clear) {
        for (const targetPath of targetConfig.paths) {
          await clearDirectoryAsync(asAbsolute(targetPath));
        }
      }
    }

    await deployPackageFiles(p, p, deployPackageConfig, vsSolution);
    await cleanEmptyDirectories(p, deployPackageConfig);
  }

  if (vsSolution) {
    await vsSolution.generate(config);
  }
}

/**
 * Copies the files of a package from the given directory, to all the configured targets. This method also builds the VS solution for
 * generating the package's scripts into DLLs.
 * @param packageDirectory The absolute path to the root directory of the package being deployed.
 * @param directory The current directory containing the files to copy to the targets.
 * @param config The package's deployment configuration.
 * @param vsSolution The Visual Studio being built. If null given, no DLL will be generated
 * @param vsProject The current Visual Studio being built for the VS solution.
 */
async function deployPackageFiles(packageDirectory: string, directory: string, config: DeployPackageConfig, vsSolution: VisualStudioSolutionBuilder, vsProject: VisualStudioProject = null) {
  const files = await readdirAsync(directory, false, true, null, false, false);

  // If DLLs are required
  if (vsSolution) {
    // Get the *.asmdef file in the current directory
    const asmdefFile = files.find(f => f.extension === 'asmdef');
    // If that file exists, start building a new VS project
    if (asmdefFile) {
      vsProject = vsSolution.addProject(await VisualStudioProject.fromAsmdef(asmdefFile.path));

      if (vsProject) {
        // For each target
        for (const targetConfig of config.targets) {
          // If the current target requires DLLs
          if (targetConfig.dll && vsProject) {
            // Add all paths to DLLs' copy targets
            for (const targetPath of targetConfig.paths) {
              vsProject.copyTargets.push(join(asAbsolute(targetPath), relative(packageDirectory, directory)));
            }
          }
        }
      }
    }
  }

  // Fix excluded file paths, and also exclude their *.meta files
  const meta = new Array<string>();
  for (let i = 0; i < config.excludeFiles.length; i++) {
    config.excludeFiles[i] = resolve(packageDirectory, config.excludeFiles[i]);
    meta.push(config.excludeFiles[i] + '.meta');
  }
  config.excludeFiles.concat(meta);

  // First pass for exculding *.meta of excluded files
  for (const file of files) {
    if (config.excludeFiles.find(p => asAbsolute(p) === file.path)) {
      config.excludeFiles.push(`${file.path}`)
      continue;
    }
  }

  // For each file of the current directory
  for (const file of files) {
    // Skip if the current file is excluded
    if (config.excludeFiles.find(p => asAbsolute(p) === file.path)) {
      continue;
    }

    // If the current file is a C# script and DLL are required
    if (vsProject && file.extension === 'cs') {
      vsProject.scripts.push(file.path);
    }

    // For each target config
    for (const targetConfig of config.targets) {
      // For each target path
      for (const targetPath of targetConfig.paths) {
        let shouldSkip = false;
        // If the current target requires DLL
        if (targetConfig.dll) {
          // Skip the current file's copy if it has an excluded extension
          for (const suffix of EXCLUDED_SUFFIXES_IF_DLL_TARGET) {
            if (file.fullName.endsWith(suffix)) {
              shouldSkip = true;
              break;
            }
          }
        }

        if(!shouldSkip) {
          await copyFileAsync(file.path, join(asAbsolute(targetPath), relative(packageDirectory, file.path)));
        }
      }
    }
  }

  const subDirs = await readdirAsync(directory, true, false, null, false, false);
  for (const subDir of subDirs) {
    await deployPackageFiles(packageDirectory, subDir.path, config, vsSolution, vsProject);
  }
}

/**
 * Removes all empty directories at the given path, and *.meta files of removed directories.
 * @param packageDirectory The absolute path to the processed package's directory.
 */
async function cleanEmptyDirectories(packageDirectory: string, config: DeployPackageConfig) {
  const dirs = await readdirAsync(packageDirectory, true, false, null, true, true);
  for (const dir of dirs) {
    const rel = relative(packageDirectory, dir.path);
    for (const target of config.targets) {
      for (const targetPath of target.paths) {
        const absTargetPath = join(asAbsolute(targetPath), rel);
        const lstat = lstatSync(absTargetPath, { throwIfNoEntry: false });
        // Cancel if the path targets an item that is not a directory or a directory that is not empty
        if (lstat && (!lstat.isDirectory() || readdirSync(absTargetPath).length > 0)) {
          continue;
        }

        // If the directory exists but is empty, remove it
        if (lstat) {
          rmdirSync(absTargetPath);
        }
        // Remove the eventual *.meta file of the directory
        rmSync(absTargetPath + '.meta', { force: true });
      }
    }
  }
}