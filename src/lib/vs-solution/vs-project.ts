import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute, readJSONFileAsync } from '@muffin-dev/node-helpers';
import { randomUUID } from 'crypto';
import { join, basename } from 'path';
import { AsmdefConfig, IAsmdefConfig } from '..';
import { TTargetDotNetFramework } from './types';

/**
 * @class Represents a Visual Studio project in a solution.
 */
export class VisualStudioProject {

  //#region Properties

  /**
   * @prop The name of the generated *.dll file, without the extension.
   */
  public dllName: string = null;

  /**
   * @prop The paths to every *.dll files to use as references in the generated DLL.
   */
  public dllReferences = new Array<string>();

  /**
   * @prop The name of Assembly Definitions to use as dependencies for this project.
   */
  public projectReferences = new Array<string>();

  /**
   * @prop Defines the target .NET framework version to use for the generated DLL.
   */
  public targetFramework: TTargetDotNetFramework = 'netstandard2.0';
  
  /**
   * @prop The paths to all the directories where the DLL should be copied after being built. Those paths are added into a file copy script
   * triggered after the build in VS succeeds.
   */
  public copyTargets = new Array<string>();

  /**
   * @prop The paths to all the scripts included into the DLL. Those scripts will be copied into the DLL directory of the VS solution.
   */
  public scripts = new Array<string>();

  /**
   * @prop The path to this project's directory. By default, uses "vsSolutionPath/dllName". If this path is relative, it's resolved from
   * the Visual Studio solution's directory.
   */
  public path: string = null;

  /**
   * @prop The path to the *.csproj file of this project. By default, uses "projectDirectoryPath/dllName.csproj". If this path is relative,
   * it's resolved from this project's directory path.
   */
  public csproj: string = null;

  /**
   * @prop The unique identifier of this project, as used in the *.sln file.
   */
  private _guid: string = null;

  /**
   * Generates a unique identifier for this project if it doesn't have one, and returns it.
   */
  public get guid(): string {
    if (this._guid === null) {
      this._guid = randomUUID().toUpperCase();
    }
    return this._guid;
  }

  /**
   * Gets the cmd command to execute when this project is built successully into a DLL from the VS solution.
   */
  public get onBuildScript(): string {
    let command = '';
    for (let i = 0; i < this.copyTargets.length; i++) {
      if (i > 0) {
        command += ' &amp;&amp; ';
      }

      /**
       * The cmd copy command: copy <source> <target>
       * 
       * The $(OutDir) and $(AssemblyName) are macros provided by the MSBuild tool, which contain respectively the path to the output
       * directory of the generated DLL (basically the /Debug or /Release directory in the VS solution), and the name of the generated
       * *.dll file (without the extension).
       * More about MSBuild macros:
       * https://docs.microsoft.com/en-us/cpp/build/reference/common-macros-for-build-commands-and-properties#list-of-common-macros
       */
      command += `copy "$(OutDir)/$(AssemblyName).dll" "${join(this.copyTargets[i], this.dllName)}.dll"`
        // Replace special chars (used for easier reading)
        .replace(/"/g, '&quot;');
    }

    return command;
  }

  //#endregion

  //#region Public API

  public static async fromAsmdef(asmdefFilePath: string) {
    asmdefFilePath = asAbsolute(asmdefFilePath);
    const asmdef = new AsmdefConfig();
    const rawAsmdef = await readJSONFileAsync(asmdefFilePath);
    overrideObject(asmdef, rawAsmdef);

    const project = new VisualStudioProject();
    project.dllName = asmdef.name;

    project.dllReferences.push('UnityEngine.dll');
    if (asmdef.includePlatforms.includes('Editor')) {
      project.dllReferences.push('UnityEditor.dll');
    }

    for (const ref of asmdef.precompiledReferences) {
      project.dllReferences.push(ref);
    }

    for (const projectRef of asmdef.references) {
      if (projectRef.toLowerCase().startsWith('guid:')) {
        console.warn(`The Assembly Definition at ${asmdefFilePath} uses GUIDs to reference other projects, so its dependencies can't be included in DLLs.`);
        break;
      }
      else {
        project.projectReferences.push(projectRef);
      }
    }

    return project;
  }

  //#endregion

}

/**
 * @interface IVisualStudioProject Represents a Visual Studio project in a solution.
 */
export interface IVisualStudioProject extends Partial<VisualStudioProject> { }