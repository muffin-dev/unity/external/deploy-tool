import { existsSync, lstatSync, mkdirSync, rmSync } from 'fs';
import { basename, join, resolve, dirname } from 'path';
import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute, copyFileAsync, readdirAsync, writeFileAsync } from '@muffin-dev/node-helpers';
import { IVisualStudioProject, VisualStudioProject } from '.';
import { IDeployConfig } from '..';
import { clearDirectoryAsync, execAsync } from '../utilities';

/**
 * @interface IVSProjectProcess Informations about a Visual Studio project being generated.
 */
interface IVSProjectProcess {
  /**
   * @prop The project data.
   */
  project: VisualStudioProject;

  /**
   * @prop The absolute path to the *.csproj file of the project.
   */
  path: string;

  /**
   * @prop The expected content of the *.csproj file.
   */
  content: string;
}

/**
 * @interface IVSSolutionPocess Informations about a Visual Studio solution being generated.
 */
interface IVSSolutionPocess {
  /**
   * @prop The projects to link to the solution.
   */
  projects: IVSProjectProcess[];

  /**
   * @prop The absolute path to the *.sln file.
   */
  path: string;
}

/**
 * @class Helper for preparing and generating a Visual Studio solution meant to generate DLLs from packages' scripts.
 */
export class VisualStudioSolutionBuilder {

  //#region Properties

  private _projects = new Array<VisualStudioProject>();

  //#endregion

  //#region Public API

  /**
   * Generates a Visual Studio solution.
   * @param path The path to the directory where you want to generate the Visual Studio solution.
   * @param managedDlls The paths to DLLs (or directories that contain *.dll files) that could be used as references by the packages.
   */
  public async generate(config: IDeployConfig) {
    // Cancel if there's no projects to generate
    if (this._projects.length <= 0) {
      return;
    }

    config.vsSolution = asAbsolute(config.vsSolution);
    await clearDirectoryAsync(config.vsSolution);
    const managedDllsMap = await this.findManagedDlls(config.managedDlls);

    const sln: IVSSolutionPocess = {
      projects: new Array<IVSProjectProcess>(),
      path: join(config.vsSolution, basename(config.vsSolution) + '.sln')
    };

    for (const project of this._projects) {
      // Skip if the project is empty
      if (project.scripts.length <= 0) {
        continue;
      }

      // Fix project path
      project.path = project.path ? resolve(config.vsSolution, project.path) : join(config.vsSolution, project.dllName);
      const projectProcess: IVSProjectProcess = {
        project: project,
        path: join(project.path, project.dllName + '.csproj'),
        content: null
      };
      project.csproj = projectProcess.path;

      // Register this project in the solution
      sln.projects.push(projectProcess);

      // Copy script files in the project directory
      for (const s of project.scripts) {
        await copyFileAsync(s, join(project.path, basename(s)));
      }
    }

    // New pass for generating the *.csproj file contents
    for (const p of sln.projects) {
      p.content = this.generateCSProjContent(p.project, managedDllsMap);
      // For each copy target used in the *.csproj file, ensure the output directory exists
      for (const t of p.project.copyTargets) {
        if (!existsSync(t)) {
          mkdirSync(t, { recursive: true });
        }
      }
    }

    // Generate the *.sln file
    await execAsync(`dotnet new sln --output="${dirname(sln.path)}"`);
    for (const p of sln.projects) {
      // Generate the *csproj file and all its required assets
      await execAsync(`dotnet new classlib --output="${dirname(p.path)}" --framework="${p.project.targetFramework}"`);
      // By default, a script "Class1.cs" is created at the same time of a project using dotnet commands, and we must remove it
      try {
        rmSync(join(dirname(p.path), 'Class1.cs'));
      } catch { }

      // Replace the *.csproj content with ours, so dependencies are already setup
      await writeFileAsync(p.path, p.content);

      // Bind the created project to the solution
      await execAsync(`dotnet sln "${sln.path}" add "${p.path}"`);
    }
    
    await execAsync(`dotnet build "${sln.path}" --force`);
  }
  
  /**
   * Checks if the given project already exists in this solution.
   * @param project The project you want to check.
   */
  public hasProject(project: IVisualStudioProject | VisualStudioProject): boolean {
    return this._projects.findIndex(p => p.dllName === project.dllName) >= 0;
  }

  /**
   * Adds a project to this solution.
   * @param project Informations about the project to add.
   * @returns Returns thr added project's instance.
   */
  public addProject(project: IVisualStudioProject | VisualStudioProject): VisualStudioProject {
    let instance: VisualStudioProject = null;
    if (project instanceof VisualStudioProject) {
      instance = project;
    }
    else {
      instance = this._projects.find(p => p.dllName === project.dllName);
      if (instance) {
        return instance;
      }
  
      instance = new VisualStudioProject();
      overrideObject(instance, project);
    }

    this._projects.push(instance);
    return instance;
  }

  //#endregion

  //#region Private API

  /**
   * Finds all the managed DLLs from directories and files listed in the managedDlls property.
   * @param managedDlls The paths to DLLs (or directories that contain *.dll files) that could be used as references by the packages.
   */
   public async findManagedDlls(managedDlls: string[]): Promise<Map<string, string>> {
    const map = new Map<string, string>();
    for (let p of managedDlls) {
      p = asAbsolute(p);
      // Skip if the item at the given path doesn't exist
      if (!existsSync(p)) {
        continue;
      }

      // If the given path leads to a directory
      if (lstatSync(p).isDirectory()) {
        // Register all *.dll files inside it
        const files = await readdirAsync(p, false, true, 'dll', true, false);
        for (const f of files) {
          map.set(basename(f.path), f.path);
        }
      }
      // Else, if the given path leads to a *.dll file, just register it
      else if (p.endsWith('.dll')) {
        map.set(basename(p), p);
      }
    }
    return map;
  }

  /**
   * Makes a *.csproj file based on the given project's settings.
   * @param project The project you want to generate the *.csproj.
   */
  private generateCSProjContent(project: VisualStudioProject, managedDllsMap: Map<string, string>): string {
    // Prepare DLL references XML content
    const dllReferencesLines = new Array<string>();
    for (const ref of project.dllReferences) {
      const item = managedDllsMap.get(ref) || null;
      if(!item) {
        console.warn(`Missing managed DLL reference "${ref}" for package DLL "${project.dllName}"`);
        continue;
      }

      dllReferencesLines.push(`<Reference Include="${ref.slice(0, -'.dll'.length)}">
      <HintPath>${item}</HintPath>
    </Reference>`);
    }

    // Prepare project references XML content
    const projectReferencesLines = new Array<string>();
    for (const ref of project.projectReferences) {
      const projectRef = this._projects.find(p => p.dllName === ref);
      if (!projectRef) {
        console.warn(`Missing DLL project reference "${ref}" for package DLL ${project.dllName}`);
        continue;
      }

      projectReferencesLines.push(`<ProjectReference Include="${projectRef.csproj}" />`)
    }

    let onBuildCommand = project.onBuildScript;
    if (onBuildCommand && onBuildCommand.length > 0) {
      onBuildCommand = `<Exec Command="${onBuildCommand}" />`;
    }

    // Yes, I didn't want to learn and use an XML library, feel free to add so if you've got the time :)
    const csprojContent = `<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>${project.targetFramework}</TargetFramework>
  </PropertyGroup>

  <ItemGroup>
    ${(projectReferencesLines.length > 0 ? projectReferencesLines.join('\n\t\t') : '')}
  </ItemGroup>

  <ItemGroup>
    ${(dllReferencesLines.length > 0 ? dllReferencesLines.join('\n\t\t') : '')}
  </ItemGroup>

  <Target Name="PostBuild" AfterTargets="PostBuildEvent">
    ${onBuildCommand}
  </Target>
  
</Project>`

    return csprojContent;
  }

  //#endregion

}