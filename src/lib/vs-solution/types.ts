/**
 * @type Defines a target .NET framework version, as it would be written in a *.csproj file.
 */
 export type TTargetDotNetFramework = 'netstandard2.0' | 'netstandard 2.1' | 'net40';