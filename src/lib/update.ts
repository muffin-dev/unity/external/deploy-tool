import { join } from 'path';
import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute, writeFileAsync } from '@muffin-dev/node-helpers';
import { DeployConfig, IDeployConfig, SemVerInfo, PackageConfig, ISemVerInfo, normalize, execAsync, PACKAGE_JSON } from '.';

/**
 * @type Defines the severity for updating the version of a package.
 */
export type TUpdateSeverity = 'major' | 'minor' | 'patch';

/**
 * @class Available options for updating a package.
 */
export class UpdatePackageOptions {

  /**
   * @prop Defines the severity for updating the version of a package.
   */
  public severity: TUpdateSeverity = 'patch';

  /**
   * @prop The commit message to use for the update changes commit. If null or undefined, the update changes won't be commited
   * automatically.
   */
  public commit: string = null;

  /**
   * @prop The message to use for the update commit's tag. If null or undefined, the update commit won't be tagged. Used only if a commit
   * message has been set.
   */
  public tag: string = null;

  /**
   * @prop If defined, overrides the default tag name applied to the update commit. Used only if a commit and the tag message have been set.
   */
  public tagName: string = null;

  /**
   * @prop If enabled, the update commit is pushed automatically at the end of the process.
   */
  public push = false;

}

/**
 * @interface IUpdatePackageOptions Available options for updating a package.
 */
export interface IUpdatePackageOptions extends Partial<UpdatePackageOptions> { }

/**
 * 
 * @param path The path to the directory of the package you want to update.
 * @param options Facultative options for updating a package.
 */
export async function update(path: string, options: IUpdatePackageOptions = null) {
  // Fix input path
  path = asAbsolute(path);

  // Fix input options
  const tmpOptions = new UpdatePackageOptions();
  options = overrideObject(tmpOptions, options);

  const packageJson = await updatePackageJsonFile(path, options.severity);
  await commitUpdateChanges(path, options, packageJson);
}

/**
 * Overwrite the package.json file of an existing package with an updated version.
 * @param path The absolute path to the directory of the package you want to update.
 * @param severity The severity of the update.
 * @returns Returns the target version string of the package.
 */
async function updatePackageJsonFile(path: string, severity: TUpdateSeverity) {
  const packageJsonFilePath = join(path, PACKAGE_JSON);
  const packageJson = await PackageConfig.read(packageJsonFilePath);
  const targetVersion = evaluateVersion(packageJson.version, severity || 'patch');
  packageJson.version = targetVersion;
  await writeFileAsync(packageJsonFilePath, JSON.stringify(packageJson, null, 2));

  return packageJson;
}

/**
 * Commits the package update changes if required.
 * @param path The path to the directory of the updated package.
 * @param config The configuration used to update the package.
 * @param options The options of the update operation.
 */
async function commitUpdateChanges(path: string, options: IUpdatePackageOptions, packageJson: PackageConfig) {
  if (!options.commit) {
    return;
  }

  // Get the Git repository directory
  let gitRepository: string = null;
  try {
    gitRepository = await execAsync('git rev-parse --show-toplevel', path);
  }
  catch (error) {
    console.error('Impossible to find the package\'s Git repository.', error);
    return;
  }

  // Add the last changes, and commit them
  try {
    await execAsync(`git add . && git commit -m \"${options.commit}\"`, gitRepository);
  }
  catch (error) {
    console.error('Failed to commit the update changes.', error);
    return;
  }

  // Add a tag
  if (options.tag) {
    try {
      await execAsync(`git tag -a -m \"${options.tag}\" \"${options.tagName ? options.tagName : evaluateTag(packageJson.name, packageJson.version)}\"`, gitRepository);
    }
    catch (error) {
      console.error('Failed to add the tag.', error);
    }
  }

  // Push the last changes
  if (options.push) {
    try {
      await execAsync(`git push`, gitRepository);
    }
    catch (error) {
      console.error('Failed to push the update commit.', error);
    }
  }
}

/**
 * Evaluates the next version of a package.
 * @param currentVersion The current package version.
 * @param severity The severity of the update.
 * @returns Returns the target version as SemVer string.
 */
export function evaluateVersion(currentVersion: string, severity: TUpdateSeverity) {
  const semver = SemVerInfo.parse(currentVersion);
  if (severity === 'major') {
    semver.major++;
    semver.minor = 0;
    semver.patch = 0;
  }
  else if (severity === 'minor') {
    semver.minor++;
    semver.patch = 0;
  }
  else {
    semver.patch++;
  }

  return `${semver.major}.${semver.minor}.${semver.patch}`;
}

/**
 * Creates a tag based on the package name to identify a package version.
 * @param name The name of the package for which you want to generate the tag.
 * @param version The version of the package at the tagged commit.
 * @returns Returns the generated tag, which looks like: package-name/major.x/major.minor.patch
 */
export function evaluateTag(name: string, version: string | ISemVerInfo) {
  name = normalize(name);
  const splitName = name.split('.');
  name = splitName[splitName.length - 1];

  if (typeof version === 'string') {
    version = SemVerInfo.parse(version);
  }
  return `${name}/${version.major}.x/${version.major}.${version.minor}.${version.patch}`;
}