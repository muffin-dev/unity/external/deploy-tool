import { existsSync, mkdirSync } from 'fs';
import { basename, join } from 'path';
import { asAbsolute, copyFileAsync, writeFileAsync } from '@muffin-dev/node-helpers';
import { DeployConfig, IDeployConfig, AsmdefConfig, IPackageConfig, PackageConfig, normalize, DEFAULT_COMPANY_NAME, PACKAGE_JSON, README_MD } from '.';

const DOCUMENTATION_DIR = 'Documentation~';
const EDITOR_DIR = 'Editor';
const RUNTIME_DIR = 'Runtime';

export async function create(path: string, infos: IPackageConfig = null, config: string | IDeployConfig = null) {
  // Fix input path
  path = asAbsolute(path);
  // Create package directory if it doesn't exist yet
  if (!existsSync(path)) {
    mkdirSync(path, { recursive: true });
  }
  // Fix config
  if (!config) {
    config = new DeployConfig();
  }
  if (typeof config === 'string') {
    config = await DeployConfig.read(asAbsolute(config));
  }
  // Fix infos
  if (!infos) {
    infos = new PackageConfig();
  }

  // Fix package unique name
  if(!infos.name) {
    infos.name = getComName(path, config.company);
  }
  // Fix package display name
  if (!infos.displayName) {
    infos.displayName = '';
    if (config.company) {
      infos.displayName = config.company + ' - ';
    }
    infos.displayName += basename(path);
  }

  if (!infos.author.name) { infos.author.name = config.author.name; }
  if (!infos.author.email) { infos.author.email = config.author.email; }
  if (!infos.author.url) { infos.author.url = config.author.url; }

  // Write package.json file using the package infos
  await makePackageJsonFile(path, infos);
  await makeReadmeFile(path, infos, config);
  await makeDocumentationDirectory(path);
  const runtimeAsmdefName = await makeRuntimeDirectory(path, infos, config);
  await makeEditorDirectory(path, runtimeAsmdefName);
}

/**
 * Creates the package.json file in the package's directory.
 * @param path The absolute path to the package's directory.
 * @param infos The informations about the package to create.
 */
async function makePackageJsonFile(path: string, infos: IPackageConfig) {
  const packageJsonFilePath = join(path, PACKAGE_JSON);
  await writeFileAsync(packageJsonFilePath, JSON.stringify(infos, null, 2));
}

/**
 * Makes the README.md file, using the given options.
 * @param path The path to the package's directory.
 * @param infos The informations about the package to create.
 * @param config The configuration used for creating the package.
 */
async function makeReadmeFile(path: string, infos: IPackageConfig, config: IDeployConfig) {
  let readmeContent = `# ${(infos.displayName ? infos.displayName : infos.name)}`;
  if (infos.description) {
    readmeContent += `\n\n${infos.description}`;
  }
  if (config.company) {
    readmeContent += `\n\n${config.company} © ${new Date().getFullYear()}`;
  }
  await writeFileAsync(join(path, README_MD), readmeContent);
}

/**
 * Makes the /Documentation~ directory with basic README.md file in it.
 * @param path The path to the package's directory.
 */
async function makeDocumentationDirectory(path: string) {
  const documentationDirectoryPath = join(path, DOCUMENTATION_DIR);
  if (!existsSync(documentationDirectoryPath)) {
    mkdirSync(documentationDirectoryPath);
  }

  const documentationReadmeFilePath = join(documentationDirectoryPath, README_MD);
  if (!existsSync(documentationReadmeFilePath)) {
    await copyFileAsync(join(path, README_MD), documentationReadmeFilePath);
  }
}

/**
 * Makes the /Runtime directory, with the minimum *.asmdef configuration.
 * @param path The path to the package's directory.
 * @param infos The informations about the package to create.
 * @param config The configuration used for creating the package.
 * @returns Returns the name of the runtime Assembly Definition (without the *.asmdef extension).
 */
async function makeRuntimeDirectory(path: string, infos: IPackageConfig, config: IDeployConfig): Promise<string> {
  const runtimeDirectoryPath = join(path, RUNTIME_DIR);
  if (!existsSync(runtimeDirectoryPath)) {
    mkdirSync(runtimeDirectoryPath);
  }

  const asmdefName = infos.displayName
    ? `${(config.company ? config.company : DEFAULT_COMPANY_NAME)}.Unity.${basename(path).replace(' ', '')}`
    : infos.name;
  const asmdefContent = new AsmdefConfig(asmdefName);
  await writeFileAsync(join(runtimeDirectoryPath, `${asmdefName}.asmdef`), JSON.stringify(asmdefContent, null, 2));
  
  return asmdefName;
}

/**
 * Makes the /Editor directory, with the minimum *.asmdef configuration.
 * @param path The path to the package's directory.
 * @param asmdefName The name of the *.asmdef file created in the /Runtime directory.
 */
async function makeEditorDirectory(path: string, asmdefName: string){
  const editorDirectoryPath = join(path, EDITOR_DIR);
  if (!existsSync(editorDirectoryPath)) {
    mkdirSync(editorDirectoryPath);
  }

  asmdefName += '.Editor';
  const asmdefContent = new AsmdefConfig(asmdefName, true);
  await writeFileAsync(join(editorDirectoryPath, `${asmdefName}.asmdef`), JSON.stringify(asmdefContent, null, 2));
}

/**
 * Gets the unique name of a package using the com notation (com.company.product).
 * @param name The name of the package, or the path to its directory.
 * @param company The name of the company that provide the package.
 */
export function getComName(name: string, company: string) {
  return `com.${normalize(company ? company : DEFAULT_COMPANY_NAME)}.${normalize(basename(name))}`
}

/**
 * Gets the default display name of a package, which looks like "Company - PackageName".
 * @param name The name of the package.
 * @param company The namd of the company that provide the package.
 */
export function getDefaultDisplayName(name: string, company: string) {
  let displayName = '';
  if (company) {
    displayName = company + ' - ';
  }
  return displayName + basename(name);
}