import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute } from '@muffin-dev/node-helpers';
import { Command } from 'commander';
import inquirer from 'inquirer';
import { UpdatePackageOptions, TUpdateSeverity, PackageConfig, update, evaluateTag, evaluateVersion } from '..';
import { requireInput } from '.';

/**
 * @class Represents the arguments data for the "update package" command.
 */
 export class UpdateCommandArgs {
  public directory: string = null;
  public severity: TUpdateSeverity = null;
  public commit: string = null;
  public tag: string = null;
  public tagName: string = null;
  public push: boolean = null;
  public silent = false;

  constructor(args: IUpdateCommandArgs) {
    overrideObject(this, args);
  }
}

/**
 * @interface IUpdateCommandArgs Represents the arguments data for the "update package" command.
 */
export interface IUpdateCommandArgs extends Partial<UpdateCommandArgs> { }

/**
 * Defines the update command arguments, options and action.
 * @param program The Command instance, from commander package.
 */
export default function(program: Command) {
  program
    .command('update')
    .argument('[directory]', 'The path to the directory of the package you want to update.')
    .option('--severity <severity>', 'The severity of the update. Possible answers are "major", "minor" and "patch"')
    .option('--commit <commit>', 'The message of the automatic commit of the update changes. If not defined, the package won\'t be comitted.')
    .option('--tag <tag>', 'The message of the update changes commit\'s tag. If not defined, no tag will be applied.')
    .option('--tagName <tagName>', 'The name of the update changes commit\'s tag.')
    .option('-p --push', 'If enabled, the update changes commit is pushed automatically at the end of the process.')
    .option('-s --silent', 'Enables silent mode. Assumes that all arguments are setup, so the wizard is skipped.')
    .action(async (directory: string, args: IUpdateCommandArgs) => {
      args = new UpdateCommandArgs(args);
      args.directory = directory;

      // If the user wants to use the wizard in case there's missing data
      if (!args.silent) {
        args = await useWizard(args);
      }

      const updateOptions = new UpdatePackageOptions();
      overrideObject(updateOptions, args);
      await update(args.directory, updateOptions);
    });
}

/**
 * Uses inquirer to ask user for informations about the package to update.
 * @param args The input arguments from the CLI.
 */
 async function useWizard(args: IUpdateCommandArgs) {
  const tmpArgs = Object.assign({ }, args);

  await requireInput(tmpArgs, 'directory', 'Directory:', null, (input: string) => asAbsolute(input));

  // Read package data
  let packageJson: PackageConfig = null;
  try {
    packageJson = await PackageConfig.read(tmpArgs.directory);
  }
  catch (error) {
    console.error(`The package.json file in "${tmpArgs.directory}" doesn't exist or can't be read.`, error);
    return;
  }

  if (!tmpArgs.severity) {
    Object.assign(tmpArgs, await inquirer.prompt([
      {
        name: 'severity',
        type: 'list',
        choices: [ 'major', 'minor', 'patch' ],
        default: 'patch',
        message: 'Severity of the update:'
      }
    ]));
  }
  
  const targetVersion = evaluateVersion(packageJson.version, tmpArgs.severity);

  await requireInput(tmpArgs, 'commit', 'Commit message (leave empty to not commit automatically):');
  if (tmpArgs.commit) {
    await requireInput(tmpArgs, 'tag', 'Tag message (leave empty to not add a tag automatically):');
    if (tmpArgs.tag) {
      await requireInput(tmpArgs, 'tagName', 'Tag name:', evaluateTag(packageJson.name, targetVersion));
    }
  }

  if (tmpArgs.push === undefined || tmpArgs.push === undefined) {
    Object.assign(tmpArgs, await inquirer.prompt([
      {
        name: 'push',
        type: 'confirm',
        default: false,
        message: 'Push changes automatically?'
      }
    ]));
  }

  // Require confirmation
  const process = {
    proceed: true
  };

  console.log(`Package will be updated at "${tmpArgs.directory}" from version ${packageJson.version} to ${targetVersion}, with the following arguments:`, tmpArgs);
  Object.assign(process, await inquirer.prompt([
    {
      name: 'proceed',
      type: 'confirm',
      message: 'Is everything alright?',
      default: process.proceed
    }
  ]));

  if (process.proceed) {
    return tmpArgs;
  }
  else {
    console.clear();
    await useWizard(args);
  }
}