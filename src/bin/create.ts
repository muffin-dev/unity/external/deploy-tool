import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute } from '@muffin-dev/node-helpers';
import { Command } from 'commander';
import inquirer from 'inquirer';
import { DeployConfig, PackageConfig, create, getComName, getDefaultDisplayName } from '..';
import { requireInput } from '.';

/**
 * @class Represents the arguments data for the "create package" command.
 */
export class CreateCommandArgs {
  public directory: string = null;
  public authorEmail: string = null;
  public authorName: string = null;
  public authorUrl: string = null;
  public company: string = null;
  public description: string = null;
  public displayName: string = null;
  public name: string = null;
  public silent = false;
  public unity: string = null;
  public version: string = null;
  public config: string = null;

  constructor(args: ICreateCommandArgs) {
    overrideObject(this, args);
  }
}

/**
 * @interface ICreateCommandArgs Represents the arguments data for the "create package" command.
 */
export interface ICreateCommandArgs extends Partial<CreateCommandArgs> { }

/**
 * Defines the create command arguments, options and action.
 * @param program The Command instance, from commander package.
 */
export default function(program: Command) {
  program
    .command('create')
    .description('Creates a new Unity package.')
    .addHelpText('after', 'Creates a new Unity package in a directory, following the layout as specified in the Unity manual: https://docs.unity3d.com/Manual/cus-layout.html')
    .argument('[directory]', 'The directory of the package you want to create.')
    .option('--authorEmail <authorEmail>', 'The email address of the package\'s author.')
    .option('--authorName <authorName>', 'The name of the package\'s author.')
    .option('--authorUrl <authorUrl>', 'The website URL of the package\'s author.')
    .option('--company <company>', 'The name of the company that provides the package.')
    .option('--description <description>', 'The description of the package.')
    .option('--displayName <displayName>', 'The display name of the package, as displayed in the Package Manager.')
    .option('--name <name>', 'The unique name of the package, using the com notation: com.company.product')
    .option('-s --silent', 'Enables silent mode. Assumes that all arguments are setup, so the wizard is skipped.')
    .option('--unity <unity>', 'The minimum required Unity version to use the package.')
    .option('--version <version>', 'The version of the package.')
    .option('--config <config>', 'The path to the deployment config file to use.')
    .action(async (directory: string, args: ICreateCommandArgs) => {
      args = new CreateCommandArgs(args);
      args.directory = directory;

      // Prepare deplloy config
      let deployConfig = new DeployConfig();
      // Read the deployment config file from cwd() if it exists
      try {
        deployConfig = await DeployConfig.read(args.config ? asAbsolute(args.config) : process.cwd());
      } catch { }

      // If the user wants to use the wizard in case there's missing data
      if (!args.silent) {
        args = await useWizard(args, deployConfig);
      }

      // Override deploy config
      if (args.authorEmail) { deployConfig.author.email = args.authorEmail; }
      if (args.authorName) { deployConfig.author.name = args.authorName; }
      if (args.authorUrl) { deployConfig.author.url = args.authorUrl; }
      if (args.company) { deployConfig.company = args.company; }

      // Prepare package config
      const packageConfig = new PackageConfig();
      // Override package config
      overrideObject(packageConfig, args);
      await create(args.directory, packageConfig, deployConfig);
    });
}

/**
 * Uses inquirer to ask user for informations about the package to create.
 * @param args The input arguments from the CLI.
 * @param config The current deployment configuration to use.
 */
async function useWizard(args: ICreateCommandArgs, config: DeployConfig) {
  const tmpArgs = Object.assign({ }, args);

  await requireInput(tmpArgs, 'directory', 'Directory:', null, (input: string) => asAbsolute(input));
  await requireInput(tmpArgs, 'company', 'Company name:', config.company || 'Company');
  await requireInput(tmpArgs, 'name', 'Package name (using com notation, com.company.product):', getComName(tmpArgs.directory, tmpArgs.company));
  await requireInput(tmpArgs, 'displayName', 'Display name (as displayed in the Packages Manager):', getDefaultDisplayName(tmpArgs.directory, tmpArgs.company));
  await requireInput(tmpArgs, 'description', 'Description:');

  if (!tmpArgs.authorName && !config.author.name) {
    Object.assign(tmpArgs, await inquirer.prompt([
      {
        name: 'authorName',
        type: 'input',
        message: 'Name of the author:'
      },
      {
        name: 'authorEmail',
        type: 'input',
        message: 'Email address of the author:',
        default: config.author.email || ''
      },
      {
        name: 'authorUrl',
        type: 'input',
        message: 'Website URL of the author:',
        default: config.author.url || ''
      }
    ]));
  }
  
  await requireInput(tmpArgs, 'version', 'Version:', '0.0.1');
  await requireInput(tmpArgs, 'unity', 'Minimum required Unity version:', (new Date().getFullYear() - 2) + '.3');

  // Require confirmation
  const process = {
    proceed: true
  };

  console.log(`Package will be created at "${tmpArgs.directory}" with the following arguments:`, tmpArgs);
  Object.assign(process, await inquirer.prompt([
    {
      name: 'proceed',
      type: 'confirm',
      message: 'Is everything alright?',
      default: process.proceed
    }
  ]));

  if (process.proceed) {
    return tmpArgs;
  }
  else {
    console.clear();
    await useWizard(args, config);
  }
}