import { overrideObject } from '@muffin-dev/js-helpers';
import { asAbsolute } from '@muffin-dev/node-helpers';
import { Command } from 'commander';
import inquirer from 'inquirer';
import { deploy } from '..';
import { DeployConfig, splitSemicolon } from '..';

export class DeployCommandArgs {
  
  public silent = false;
  public config: string = null;
  private _directories = new Array<string>();

  public get directories(): string[] {
    return this._directories;
  }

  public set directories(value: string|string[]) {
    this._directories = Array.isArray(value) ? value : [ value ];
  }

  constructor(args: IDeployCommandArgs) {
    overrideObject(this, args);
  }

}

export interface IDeployCommandArgs extends Partial<DeployCommandArgs> { }

export default function(program: Command) {
  program
    .command('deploy')
    .description('Deploys existing Unity packages.')
    .addHelpText('after', 'This operation requires the wizard or complete informations from the deploy configuration files.')
    .argument('[directories]', 'The directory (or directories, separated by semicolons) of the packages you want to deploy.')
    .option('-s --silent', 'Enables silent mode. Assumes that the configuration file is correctly setup, so the wizard is skipped.')
    .option('--config <config>', 'The path to the deployment config file to use.')
    .action(async (directories: string|string[], args: IDeployCommandArgs) => {
      // Fix args
      if (!Array.isArray(directories)) {
        directories = splitSemicolon(directories);
      }
      args = new DeployCommandArgs(args);
      args.directories = directories;

      // Prepare deplloy config
      let deployConfig = new DeployConfig();
      // Read the deployment config file from cwd() if it exists
      try {
        deployConfig = await DeployConfig.read(args.config ? asAbsolute(args.config) : process.cwd());
      } catch { }

      if (!args.silent) {
        await useWizard(args, deployConfig);
      }

      await deploy(directories, deployConfig);
    });
}

/**
 * Uses inquirer to ask user for informations about the package(s) to deplloy.
 * @param args The input arguments from the CLI.
 * @param config The current deployment configuration to use.
 */
async function useWizard(args: IDeployCommandArgs, config: DeployConfig) {
  console.warn('@todo Deploy command wizard');

  const tmpArgs = new DeployCommandArgs(args);

  // Require confirmation
  const process = {
    proceed: true
  };

  console.log(tmpArgs);
  Object.assign(process, await inquirer.prompt([
    {
      name: 'proceed',
      type: 'confirm',
      message: 'Is everything alright?',
      default: process.proceed
    }
  ]));

  if (process.proceed) {
    return tmpArgs;
  }
  else {
    console.clear();
    await useWizard(args, config);
  }
}

// import { existsSync } from 'fs';
// import { join } from 'path';
// import { asAbsolute } from '@muffin-dev/node-helpers';
// import { Command } from 'commander';
// import inquirer from 'inquirer';
// import { deployPackage, DEPLOY_CONFIG, IDeployConfig, IDeployPackageSettings, readConfig, splitSemicolon } from '../lib';

// export default function(program: Command) {
//   program
//     .command('deploy')
//     .description('Deploys an existing Unity package')
//     .addHelpText('after', 'This command exclusively uses the deployment config files from the directories of the packages you want to deploy.')
//     .argument('[directories]', 'The directory (or directories, separated by semicolons) of the package(s) you want to deploy.')
//     .option('-v, --vsSolution <path>', 'Defines the directory that will contain the Visual Studio solution generated for building DLLs.')
//     .option('-s, --silent', 'Assumes that all the arguments are set, so it won\'t use command prompts to ask you these informations.')
//     .action(async (directories: string | string[], options: any) => {
//       const settings = new Array<IDeployPackageSettings>();

//       if (!directories) {
//         directories = new Array<string>();
//       }

//       if (typeof directories === 'string') {
//         directories = splitSemicolon(directories);
//       }

//       for (let i = 0; i < directories.length; i++) {
//         directories[i] = asAbsolute(directories[i]);
//       }

//       // If silent mode is disabled
//       if (!options.silent) {

//         // If the user already defined a directory
//         if (directories.length > 0) {
//           Object.assign(settings, await inquirer.prompt([
//             {
//               name: 'continue',
//               type: 'confirm',
//               message: 'Do you want to deploy another package at the same time?',
//               default: false
//             }
//           ]));
//         }

//         while ((settings as any).continue || (settings as any).continue === undefined) {
//           Object.assign(settings, await inquirer.prompt([
//             {
//               name: 'pendingDirectory',
//               type: 'input',
//               message: 'Select the directory of a package you want to deploy:',
//               filter: (input: string) => asAbsolute(input)
//             },
//             {
//               name: 'continue',
//               type: 'confirm',
//               message: 'Do you want to deploy another package at the same time?',
//               default: false
//             }
//           ]));

//           if ((settings as any).pendingDirectory && (settings as any).pendingDirectory !== '') {
//             directories.push((settings as any).pendingDirectory);
//           }
//         }

//         // Delete tmp properties
//         delete (settings as any).pendingDirectory;
//         delete (settings as any).continue;

//         if (!options.vsSolution) {
//           Object.assign(options, await inquirer.prompt([
//             {
//               name: 'vsSolution',
//               type: 'input',
//               message: 'Select the directory that will contain he generated Visual Studio solution (for building DLLs):',
//               filter: (input: string) => asAbsolute(input)
//             },
//           ]));
//         }

//         console.log('The command will deploy the following packages:', directories);
//         console.log('The visual studio solution will be generated at:', options.vsSolution);
//         Object.assign(settings, await inquirer.prompt([
//           {
//             name: 'proceed',
//             type: 'confirm',
//             message: 'Is everything alright?',
//             default: true
//           }
//         ]));
    
//         if (!(settings as any).proceed) {
//           return;
//         }
//       }

//       const errors = new Array<string>();
//       for (let i = 0; i < directories.length; i++) {
//         const packageJsonFilePath = join(directories[i], 'package.json');
//         if (!existsSync(packageJsonFilePath)) {
//           errors.push(`The directory "${directories[i]}" doesn't contain a package.`);
//           continue;
//         }

//         const deployConfigFilePath = join(directories[i], DEPLOY_CONFIG);
//         if (!existsSync(deployConfigFilePath)) {
//           errors.push(`The package at "${directories[i]}" doesn't have deployment instructions. Please add a ${DEPLOY_CONFIG} file.`);
//           continue;
//         }

//         let deployConfig: IDeployConfig = null;
//         try {
//           deployConfig = await readConfig(deployConfigFilePath);
//         }
//         catch (error) {
//           errors.push(`Impossible to read the deployment instructions from file at "${deployConfigFilePath}": ${(error ? (error as Error).message || error.toString() : 'Unknown error')}`);
//           continue;
//         }

//         if (!deployConfig.deploy) {
//           errors.push(`No deployment instructions found from config file at "${deployConfigFilePath}".`);
//           continue;
//         }

//         for (let i = 0; i < deployConfig.deploy.length; i++) {
//           settings.push({
//             directory: directories[i],
//             targets: deployConfig.deploy[i].targets || null,
//             clear: deployConfig.deploy[i].clear || false,
//             dll: deployConfig.deploy[i].dll || false,
//             dllReferences: deployConfig.deploy[i].dllReferences || null,
//             dllTargetFramework: deployConfig.deploy[i].dllTargetFramework || null,
//           });
//         }
//       }

//       for (let e of errors) {
//         console.warn(e);
//       }
//       await deployPackage(settings, options.vsSolution);
//     });
// }