import inquirer from 'inquirer';

/**
 * Asks the user for an input in the terminal to match an argument that is not set already.
 * @param args The current state of a command arguments.
 * @param argName The name of the argument you want to check.
 * @param message The message to display for the input request.
 * @param defaultValue The default value of the argument.
 * @param filter The filter to apply to the input value.
 */
 export async function requireInput(args: any, argName: string, message: string, defaultValue: string = null, filter: (input: string) => string = null) {
  if (!args[argName]) {
    Object.assign(args, await inquirer.prompt([
      {
        name: argName,
        type: 'input',
        message: message || undefined,
        default: defaultValue || undefined,
        filter: filter || undefined
      }
    ]));
  }
}