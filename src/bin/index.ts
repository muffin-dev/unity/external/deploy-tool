#!/usr/bin/env node
import { Command } from 'commander';

export * from './utilities';

import CreateCommand from './create';
import UpdateCommand from './update';
import DeployCommand from './deploy';

const program = new Command('unity-deploy-tool');

CreateCommand(program);
UpdateCommand(program);
DeployCommand(program);

program.parse(process.argv);